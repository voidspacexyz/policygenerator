# PolicyGenerator

A basic policy generation with all elementary info captured and also the ability to style the document

## Requirements 

1. Pandoc (https://pandoc.org/)


## Steps


1. pandoc --standalone --template template/base_structure.html policies/infosec.md > output/infosec.html
