---
title: Information Policy Document
current_publish_date: July 22, 2021
versions:
- version: 1.1
  change: This is a short summary about the changes happened in 1.1
  change_by: username_chaed_by 
  approved_by: approv_by
- version: 1.0
  change: Thi is a short summary about the changes happened in 1.1
  change_by: usname_chaed_by 
  approved_by: aprov_by

---

1. Scope

The document covers the parts where all forms of PII is collected

2. Data Collected

The app collects the following data

* Email
* Address
* Contact

3. Data Sharing 

The collected data will not be shared with anybody other than the <Company Name here> without the consent of the user. 

